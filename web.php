<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/getData', 'EventController@getData')->name('getData');
Route::get('/getData/{id}', 'EventController@getDetailEvent')->name('getDetailEvent');
Route::get('/getComment/{event_id}', 'CommentController@getComment')->name('getComment');
Route::post('/addEvent', ['as' => 'postAddEvent', 'uses' => 'EventController@postAddEvent']);
Route::post('/editevent/{id}', ['as' => 'postEditEvent', 'uses' => 'EventController@postEditEvent']);
Route::post('/comment', ['as' => 'comment', 'uses' => 'CommentController@comment']);
Route::post('/re_comment', ['as' => 're_comment', 'uses' => 'CommentController@re_comment']);
Route::post('/deletecomment', ['as' => 'deletecomment', 'uses' => 'CommentController@deletecomment']);
Route::post('/deleterecomment', ['as' => 'deleterecomment', 'uses' => 'CommentController@deleterecomment']);
Route::post('/deleteevent', ['as' => 'deleteevent', 'uses' => 'EventController@deleteevent']);
Route::get('change-language/{language}', 'HomeController@changeLanguage')->name('user.change-language');

// get title for autocomplade
Route::get('gettitle',[
	'as'=>'gettitle',
	'uses'=>'QuickEventController@gettitle'
]);
Route::get('getSearch',[
	'as'=>'getSearch',
	'uses'=>'QuickEventController@getSearch'
]);

// post and edit event
Route::post('addquickevent',[
	'as'=>'postAddQuickEvent',
	'uses'=>'QuickEventController@postAddQuickEvent'
]);
Route::post('editquichevent/{id}',[
	'as'=>'postEditQuickevent',
	'uses'=>'QuickEventController@postEditQuickevent'
]);
//delete event
Route::get('delete-event/{id}',[
	'as'=>'deleteevent',
	'uses'=>'QuickEventController@deleteevent'
]);
//resize
Route::post('resize-event/{id}',[
	'as'=>'postResizeEvent',
	'uses'=>'QuickEventController@postResizeEvent'
]);
//drop
Route::post('drop-event/{id}',[
	'as'=>'postdropEvent',
	'uses'=>'QuickEventController@postdropEvent'
]);
//post history

Route::group(['prefix'=>'history'],function(){
	Route::post('posthistory-edit-event/{id}',[
		'as'=>'postHistory',
		'uses'=>'HistoryController@postHistory'
	]);
	Route::post('posthistory-resize-event/{id}',[
		'as'=>'postResize',
		'uses'=>'HistoryController@postResize'
	]);
	Route::post('posthistory-drop-event/{is}',[
		'as'=>'postDrop',
		'uses'=>'HistoryController@postDrop'
	]);
	Route::get('getHistory/{id}',[
		'as'=>'getHistory',
		'uses'=>'HistoryController@getHistory'
	]);
});







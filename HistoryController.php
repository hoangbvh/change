<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History;
class HistoryController extends Controller
{
    public function getHistory($id){
       $event_old = History::where('event_id',$id)->get();
       return response()->json([
            'event_old'=>$event_old
        ]
        );
    } 
    public function postHistory(Request $req,$id){
    	$event_old = new History;
        $event_old->title_old = $req->title_old;
        $event_old->event_id = $id;
        $event_old->description_old = "khong co";
        $event_old->status_id = 1;
        $event_old->user_id = 1; 
        if($req->allDay == 'true'){
            $event_old->start_old = explode(" ", $req->start_old)[0];
            $event_old->end_old = explode(" ", $req->end_old)[0];
        }     
        else {
             $event_old->start_old = $req->start_old;
             $event_old->end_old = $req->end_old;
        }
        $event_old->repeat_type = "norepeat ";
        $event_old->repeat_group = "asd";
        $event_old->save();
        //dd(gettype($req->allDay));
    }
    public function postResize(Request $req,$id){
        $event_old = new History;
        $event_old->title_old = $req->title_old;
        $event_old->event_id = $id;
        $event_old->description_old = "khong co";
        $event_old->status_id = $req->status_id_resize_old;
        $event_old->user_id = 1;
        $event_old->start_old = $req->timestart_old;
        $event_old->end_old = $req->resize_time_old;
        $event_old->repeat_type = "norepeat ";
        $event_old->repeat_group = "asd";
        $event_old->save();
    }
    public function postDrop(Request $req,$id){
        $event_old = new History;
        $event_old->title_old = $req->title_drop_old;
        $event_old->event_id = $id;
        $event_old->description_old = "khong co";
        $event_old->status_id = $req->status_id_old;
        $event_old->user_id = 1;
        $event_old->start_old = $req->timestart_drop_old;
        $event_old->end_old = $req->timeend_drop_old;
        $event_old->repeat_type = "norepeat ";
        $event_old->repeat_group = "asd";
        $event_old->save();
    }

}


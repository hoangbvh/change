<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;
use App\History;
class QuickEventController extends Controller
{
    public function gettitle(){
        $event= Events::select('title','start','end')->get();
        echo json_encode($event);
    }
    public function getSearch(Request $req){
        $event = Events::where('title',$req->title_search)->first();
        return response()->json([
            
            'event' =>$event
        ]
        );
    }
    public function postAddQuickEvent(Request $req){
        $range_noti = date('Y-m-d H:i:s', strtotime('-30 minutes',strtotime($req->start)));
        $event = new Events;
        if($req->allDay == 'true'){
            $event->allDay =1;
            $event->start = explode(" ", $req->start)[0];
            $event->end = explode(" ", $req->end)[0];
            $range_noti = date('Y-m-d H:i:s', strtotime('-1 days',strtotime($req->start)));
        }     
        else {
             $event->start = $req->start;
             $event->end = $req->end;
             $event->allDay =0; 
        }
        $event->title = $req->title;
        $event->files = "";
        $event->group_id = 1;
        $event->noti = 30;
        $event->type_noti ="m";
        $event->range_noti =$range_noti;
        $event->description = "khong co";
        $event->status_id = $req->status_id;
        $event->user_id = 1;
        $event->manager_id = 1;
        
        $event->repeat_type = "norepeat ";
        $event->repeat_group = "asd";
        $event->detail = "das";
        $event->save();
        //dd(gettype($req->allDay));
    }
    public function deleteevent($id){
    	$event = Events::find($id);
    	$event->delete();
    }
    public function postEditQuickevent(Request $req ,$id){

        $event = Events::find($id);
        $event->title = $req->title_edit;
        if($req->allDay == 'true'){
            $event->start = explode(" ", $req->start_edit)[0];
            $event->end = explode(" ", $req->end_edit)[0];
        }     
        else {
             $event->start = $req->start_edit;
             $event->end = $req->end_edit;
        }
        $event->status_id = $req->status_id;
        $event->save();    

    }
    public function postResizeEvent(Request $req, $id){
        $event = Events::find($id);
        $event->end = $req->resize_time;
        $event->status_id = $req->status_id_resize;
        $event->save(); 
    }
    public function postdropEvent(Request $req, $id){
        $event = Events::find($id);
        $event->start = $req->drop_start;
        $event->end = $req->drop_end;
        $event->status_id = $req->status_id_drop;
        $event->save(); 

    }
}

    function check_notify(argument) {
        var date_now = new Date();
        $.ajax({
            url: 'http://localhost/Full-Calendar/public/getData',
            dataType: 'json',
            success: function(data) {
                $.each(data.events, function(index, value) {
                    var offset = new Date(value.range_noti).getTime() - date_now.getTime();
                    if (offset > 0) {
                        var interval = setInterval(function() {
                            show_noti(value.title);
                            clearInterval(interval);
                        }, offset);
                    }
                });

                function show_noti(content) {
                    notify('Sự kiện sắp tới ' + content);
                    var audio = new Audio('audio/demonstrative.mp3');
                    audio.play();
                }
            }
        });
    }

    function notify(content) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            // title: 'Bootstrap notify',
            message: content,
            target: '_blank'
        }, {
            // settings
            element: 'body',
            position: null,
            type: "success",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "top",
                align: "right"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            onShow: null,
            onShown: null,
            onClose: null,
            onClosed: null,
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }

    function add_detail_event() {
        $('#calendar').fullCalendar('events');
        console.log(213);
        $('.detail_event').preloader('remove');
        var datas = new getdata();
        var checked = false;
        var status = datas.status;
        var user = datas.user;
        var html = "";
        var html_user = "";
        if (status.length > 0) {
            for (var i = 0; i < status.length; i++) {
                html = html + "<option value='" + status[i].id + "'>" + status[i].name + "</option>";
            }
        }
        if (user.length > 0) {
            for (var i = 0; i < user.length; i++) {
                html_user = html_user + "<option value='" + user[i].id + "'>" + user[i].name + "</option>";
            }
        }
        $("input[name$='date_add_from']").val(moment().format('DD-MM-YYYY'));
        $("input[name$='time_add_from']").val(moment().format('h:mm a'));
        $("input[name$='date_add_to']").val(moment().format('DD-MM-YYYY'));
        $("input[name$='time_add_to']").val(moment().add(30, 'm').format('h:mm a'));
        $('.select_status').html(html);
        $('.select_user').html(html_user);
        $('.select_manager').html(html_user);

    }

    function edit_event() {
        event.preventDefault();
        var id = $("input[name$='idEvent']").val();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var csrf = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'editevent/' + id,
            type: 'POST',
            dataType: "JSON",
            data: $("#edit_event_form").serialize(),
            // contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                // setting a timeout
                $('.detail_event').preloader();
            },
            success: function(data, status) {
                if (data.success) {
                    var event = data.data;
                    $("#calendar").fullCalendar('removeEvents', id);
                    $("#calendar").fullCalendar('renderEvent', event,
                        true);
                    $("#calendar").fullCalendar('refetchEvents');
                    $('.detail_event').preloader('remove');
                    $("#showdetail").modal('hide');
                    notify(data.message);
                }
            },
            error: function(xhr, desc, err) {
                $('.detail_event').preloader('remove');

            }
        });
    }

    function delete_events() {
        var id = $('#idEvent').val();
        var repeat_group = $("input[name$='repeat_group']").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'deleteevent/',
            type: 'POST',
            dataType: "JSON",
            data: {
                id: id,
                repeat_group: repeat_group
            },
            beforeSend: function() {
                // setting a timeout
                $('.detail_event').preloader();
            },
            success: function(data, status) {
                if (data.success) {
                    var event = data.data;
                    $("#calendar").fullCalendar('refetchEvents');
                    $("#showdetail").modal('hide');
                    notify(data.message);
                }
            },
            error: function(xhr, desc, err) {
                $('.detail_event').preloader('remove');

            }
        });
    }

    function getdata() {
        var result = "";
        $.ajax({
            url: "http://localhost/Full-Calendar/public/getData",
            async: false,
            success: function(data) {
                result = data;
            }
        });
        return result;
    }

    function getComments() {
        var id = $('#myModaledit #idEvent').val();
        $.get("http://localhost/Full-Calendar/public/getComment/" + id, function(data) {
            var current_id = data.current_id;
            var comments = data.comments;
            var recomments = data.recomments;
            var html_comments = "";
            var html_recomments = "";
            for (var i = 0; i < comments.length; i++) {
                html_comments = html_comments + '<li class="box-comment-reply">' +
                    '<div class="comment-main-level">' +
                    '<div class="comment-avatar">' +
                    '<img src="' + comments[i].avatar + '" alt="">' +
                    '</div>' +
                    '<div class="comment-box">' +
                    '<div class="comment-head">' +
                    '<h6 class="comment-name"><a href="">' + comments[i].name + '</a></h6>' +
                    '<span>' + comments[i].created_at + '</span><i class="fa fa-reply"></i><i class="fa fa-heart"></i>' +
                    (comments[i].user_id == current_id ? '<div class="delete_comment">' +
                        '<button onclick="delete_comment(' + comments[i].id + ')">Xóa</button>' +
                        '</div>' : '') +
                    '</div>' +
                    '<div class="comment-content">' +
                    comments[i].content +
                    '<div align="right">' +
                    '<a role="button" data-toggle="collapse" href="#re_comment_' + comments[i].id + '" aria-expanded="false" aria-controls="re_comment_' + comments[i].id + '">Trả lời</a>' +
                    '</div>' +
                    '<div class="collapse" id="re_comment_' + comments[i].id + '">' +
                    '<div class="well">' +
                    '<input type="hidden" id="id_comment" value="' + comments[i].id + '"></input>' +
                    '<textarea name="re_comment" class="form-control" rows="3" id="re_content_' + comments[i].id + '"></textarea>' +
                    '<div align="right">' +
                    '<button onclick="send_re_comment(' + comments[i].id + ')" class="btn btn-primary">Gửi</button>' +
                    '</div>' +
                    '</div>' +
                    '</div></div></div></div>' +
                    '<ul class="comments-list reply-list_' + comments[i].id + '">' +
                    '</ul>' +
                    '</li>';
            }
            $(".comments-list").html(html_comments);
            for (var j = 0; j < recomments.length; j++) {
                html_recomments = '<li>' +
                    '<div class="comment-avatar"><img src="' + recomments[j].avatar + '" alt=""></div>' +
                    '<div class="comment-box">' +
                    '<div class="comment-head">' +
                    '<h6 class="comment-name"><a href="">' + recomments[j].name + '</a></h6>' +
                    '<span>' + recomments[j].created_at + '</span>' +
                    '<i class="fa fa-reply"></i>' +
                    '<i class="fa fa-heart"></i>' +
                    (recomments[j].user_id == current_id ? '<div class="delete_comment">' +
                        '<button onclick="delete_re_comment(' + recomments[j].id + ')">Xóa</button>' +
                        '</div>' : '') +
                    '</div>' +
                    '<div class="comment-content">' +
                    recomments[j].content +
                    '</div>' +
                    '</div>' +
                    '</li>';
                $(".reply-list_" + recomments[j].comment_id).append(html_recomments);
            }
        });
    }

    function show_update_event() {
        $("#myModaledit").modal('hide');
        $("#showdetail").modal('show');
        var id = $('#myModaledit #idEvent').val();
        $.get("http://localhost/Full-Calendar/public/getData/" + id, function(data) {   
            $("input[name$='title']").val(data.title);
            $("input[name$='idEvent']").val(data.id);
            $("input[name$='repeat_group']").val(data.repeat_group);
            $("input[name$='date_from']").val(moment(data.start).format('DD-MM-YYYY'));
            $("input[name$='time_from']").val(moment(data.start).format('h:mm a'));
            $("input[name$='date_to']").val(moment(data.end).format('DD-MM-YYYY'));
            $("input[name$='time_to']").val(moment(data.end).format('h:mm a'));
            if (data.allDay == 0) {
                $("input[name$='allDay']").attr('checked', false);
            } else {
                $("input[name$='allDay']").attr('checked', true);
            }
            $("input[name$='noti']").val(data.noti);
            $("input[name$='file_current']").val(data.files);
            CKEDITOR.instances['description'].setData(data.description);
            var datas = new getdata();
            var comments = new getComments();
            var checked = false;
            var type_noti = data.type_noti;
            var status = datas.status;
            var repeat_type = data.repeat_type;
            var user = datas.user;
            var html = "";
            var html_user = "";
            if (status.length > 0) {
                for (var i = 0; i < status.length; i++) {
                    html = html + "<option value='" + status[i].id + "'>" + status[i].name + "</option>";
                }
            }
            if (user.length > 0) {
                for (var i = 0; i < user.length; i++) {
                    html_user = html_user + "<option value='" + user[i].id + "'>" + user[i].name + "</option>";
                }
            }

            $('.select_status').html(html);
            $('.select_user').html(html_user);
            $('.select_manager').html(html_user);
            $('.type_noti').each(function() {
                $(this).find('option[value="' + type_noti + '"]').prop('selected', true);
            });
            $('.repeat_type').each(function() {
                $(this).find('option[value="' + repeat_type + '"]').prop('selected', true);
            });
            $('.select_status').each(function() {
                $(this).find('option[value="' + data.status_id + '"]').prop('selected', true);
            });
            $('.select_user').each(function() {
                $(this).find('option[value="' + data.user_id + '"]').prop('selected', true);
            });
            $('.select_manager').each(function() {
                $(this).find('option[value="' + data.manager_id + '"]').prop('selected', true);
            });
        });
        // Edit Event Form
        $.ajax({
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            type: 'get',
            url: 'http://localhost/Full-Calendar/public/history/getHistory/'+id,
            success: function(data){
                console.log(data.event_old['0']['title_old']);
               
                var x=data.event_old;
                var xx,a,b,c,d;
                for(var i=0; i<x.length;i++){
                     xx = xx+ '<tr>'+
                        '<td>' + data.event_old[i]['title_old'] + '</td>'+'<td>' + data.event_old[i]['start_old'] +'</td>'+
                        '<td>' + data.event_old[i]['end_old'] + '</td>'+'<td>' + data.event_old[i]['description_old'] +'</td>'+'</tr>'
                    

                     $('tbody').html(xx);

                }
            }
        });


    }
 
    function saveEvent() {
        var csrf = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: 'http://localhost:8000/addEvent',
            type: 'POST',
            dataType: "JSON",
            data: {
                title: $('#eventTitle').val(),
                start: $('#date_from').val() + ' ' + $('#time_from').val(),
                end: $('#date_to').val() + ' ' + $('#time_to').val(),
                '_token': csrf
            },
            success: function(data, status) {

            },
            error: function(xhr, desc, err) {


            }
        });
    }

    // Send Comment
    function send_comment() {
        var id = $('#myModaledit #idEvent').val();
        var content = $('#comment').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'comment',
            type: 'POST',
            dataType: "JSON",
            data: {
                event_id: id,
                content: content
            },
            success: function(data, status) {
                getComments();
                $('#comment').val("");

            },
            error: function(xhr, desc, err) {


            }
        });
        event.preventDefault();
    }
    // function send recomments
    function send_re_comment(comment_id) {
        var content = $('#re_content_' + comment_id).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 're_comment',
            type: 'POST',
            dataType: "JSON",
            data: {
                comment_id: comment_id,
                content: content
            },
            success: function(data, status) {
                getComments();
                $('#re_content_' + comment_id).val("")
            },
            error: function(xhr, desc, err) {


            }
        });
        event.preventDefault();
    }

    // function deletecomment
    function delete_comment(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'deletecomment',
            type: 'POST',
            dataType: "JSON",
            data: {
                comment_id: id,
            },
            success: function(data, status) {
                getComments();
            },
            error: function(xhr, desc, err) {


            }
        });
        event.preventDefault();
    }

    function delete_re_comment(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'deleterecomment',
            type: 'POST',
            dataType: "JSON",
            data: {
                id: id,
            },
            success: function(data, status) {
                getComments();
            },
            error: function(xhr, desc, err) {


            }
        });
        event.preventDefault();
    }

    function checkRepeat() {
        var repeat = $('select[name=repeat_type]').val();
        if (repeat == 'norepeat') {
            $('.repeat_range').addClass('hide');
        } else {
            $('.repeat_range').removeClass('hide');
        }
    }
    $(document).ready(function() { // document ready
        check_notify();
        $("select[name=repeat_type]").change(function() {
            checkRepeat();
        });
        $('#detailEvent').on('shown.bs.modal', function() {
            $('#eventTitle').val("");
            $('.time_to').removeClass('hide');
            $('.time_from').removeClass('hide');
            $("#allDay").prop("checked", false);
        });
        // Validate Form Add Event Full
        $.validator.addMethod('filesize', function(value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, function(size) {
            return "MAX SIZE " + filesize(size, {
                exponent: 2,
                round: 1
            });
        });
        $("#add_event_form").validate({
            rules: {
                title: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                description: {
                    maxlength: 600
                },
                files: {
                    filesize: 10000 * 1024
                }
            },
            messages: {
                title: {
                    required: 'Tên sự kiện không được bỏ trống!',
                    minlength: 'Tên sự kiện trên 3 kí tự',
                    maxlength: 'Tên sự kiện phải nhỏ hơn 255 kí tự'
                },
                description: {
                    maxlength: 'Độ dài không được vượt quá 600 kí tự'
                },
                files: {
                    filesize: 'File không được lớn hơn 2MB'
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
            }
        });
        $("#edit_event_form").validate({
            rules: {
                title: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                description: {
                    maxlength: 600
                },
                files: {
                    filesize: 10000 * 1024
                }
            },
            messages: {
                title: {
                    required: 'Tên sự kiện không được bỏ trống!',
                    minlength: 'Tên sự kiện trên 3 kí tự',
                    maxlength: 'Tên sự kiện phải nhỏ hơn 255 kí tự'
                },
                description: {
                    maxlength: 'Độ dài không được vượt quá 600 kí tự'
                },
                files: {
                    filesize: 'File không được lớn hơn 2MB'
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
            }
        });


        // // onClick show modal detail event
        // $('#show_detail_event').on('click', function(e) {
        //     console.log(123);
        //     e.preventDefault();
        //     $("#myModaledit").modal('hide');
        //     $("#showdetail").modal('show');
        // });
        // Check allDay
        $('#allDay').change(function() {
            var checked = $('#allDay').is(":checked");
            if (checked) {
                $('#time_from').val("00:00");
                $('#time_to').val("00:00");
                $('.time_to').addClass('hide');
                $('.time_from').addClass('hide');
                $('#eventAllDay').val(checked)
            } else {
                $('.time_to').removeClass('hide');
                $('.time_from').removeClass('hide');
                $('#eventAllDay').val(checked)
            }
        });

        $('#saveEvent').on('click', function(e) {
            e.preventDefault();
            if ($('#eventAllDay').val() == 'true') {
                isAllday = true;
            } else {
                isAllday = false
            };
            saveEvent();
            var data = {
                title: $('#eventTitle').val(),
                start: $('#date_from').val() + ' ' + $('#time_from').val(),
                end: $('#date_to').val() + ' ' + $('#time_to').val(),
                allDay: isAllday
            };
            // doSubmit(data);
        });

        // Submit Add Event Form
        $("#add_event_form").submit(function(event) {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var form = $(this);
            var csrf = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: 'http://localhost:8000/addEvent',
                type: 'POST',
                dataType: "JSON",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    // setting a timeout
                    $('#detail_event').preloader();
                },
                success: function(data, status) {
                    if (data.success) {
                        $("#detailEvent").modal('hide');
                        $('#detail_event').preloader('remove');
                        $("#calendar").fullCalendar('refetchEvents');
                        notify(data.message);
                    }
                },
                error: function(xhr, desc, err) {
                    $('.detail_event').preloader('remove');
                }
            });
            /* stop form from submitting normally */

            event.preventDefault();
        });

    });